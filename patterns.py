from abc import ABC


class Strategy(ABC):
    def find_treasure(self, treasure_map):
        raise NotImplementedError


class Context:
    def __init__(self, strategy: Strategy):
        self.strategy = strategy

    def find_treasure(self, treasure_map):
        return self.strategy.find_treasure(treasure_map)
