from typing import Tuple

from patterns import Strategy


class CellWalkerFinder(Strategy):
    def __init__(self, start_corner: Tuple[int, int] = (0, 0)):
        self.start_corner = start_corner

    def find_treasure(self, treasure_map):
        row, col = self.start_corner
        seen_point = set()

        while True:

            if (row, col) in seen_point:
                return None
            seen_point.add((row, col))

            point = treasure_map[row][col]
            if (row, col) == divmod(point - 11, 10):
                return point

            row, col = divmod(point - 11, 10)
