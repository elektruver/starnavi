def recursion_find_treasure(treasure_map, previous_point=None, current_position=(0, 0), seen=set()):
    i, j = current_position
    point = treasure_map[i][j]

    if previous_point == point:
        seen.clear()
        return point

    if point in seen:
        seen.clear()
        return None
    seen.add(point)

    next_position = divmod(point - 11, 10)

    return recursion_find_treasure(treasure_map, point, next_position, seen)
