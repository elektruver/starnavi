from patterns import Strategy


class Node:
    def __init__(self, data=None):
        self.data = data
        self.next = None

    def __repr__(self):
        if self.next:
            next_ = self.next.data
        else:
            next_ = None
        return f"<{self.data} -> {next_}>"


class TreasureLinkedList(Strategy):
    def __init__(self):
        self.head = None

    def __repr__(self):
        nodes = []
        curr = self.head
        while curr:
            nodes.append(repr(curr))
            curr = curr.next
        return '[' + ', '.join(nodes) + ']'

    def clear(self):
        self.head = None

    def append(self, data):
        if not self.head:
            self.head = Node(data=data)
            return
        curr = self.head
        while curr.next:
            curr = curr.next
        curr.next = Node(data=data)

    def _build_liked_list(self, treasure_map):
        row, col = 0, 0
        seen_point = set()

        while True:

            if (row, col) in seen_point:
                break
            seen_point.add((row, col))

            point = treasure_map[row][col]
            self.append(point)

            row, col = divmod(point - 11, 10)

    def find_treasure(self, treasure_map):
        self._build_liked_list(treasure_map)

        curr = self.head
        while curr:
            if curr.next and curr.data == curr.next.data:
                return curr.data
            curr = curr.next
