def graph_find_treasure(treasure_map):
    graph = build_graph(treasure_map)

    for key, val in graph.items():
        if key == val:
            return val


def build_graph(treasure_map):
    graph = {}

    for row in treasure_map:
        for col in row:
            next_vertex_position = divmod(col - 11, 10)
            next_vertex = treasure_map[next_vertex_position[0]][next_vertex_position[1]]
            graph[col] = next_vertex

    return graph
