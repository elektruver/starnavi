from tests import BaseTestCase
from treasure_maps import FIRST_MAP, SECOND_MAP, WRONG_MAP
from algorithms import (
    CellWalkerFinder,
    graph_find_treasure,
    TreasureLinkedList,
    recursion_find_treasure
)


# python -m unittest

class TestTreasureFinders(BaseTestCase):

    def test_graph_finder(self):
        point = graph_find_treasure(FIRST_MAP)
        self.assertEqual(52, point)

        point = graph_find_treasure(SECOND_MAP)
        self.assertEqual(43, point)

        point = graph_find_treasure(WRONG_MAP)
        self.assertIsNone(point)

    def test_cell_walker_finder(self):
        context = CellWalkerFinder()

        point = context.find_treasure(FIRST_MAP)
        self.assertEqual(52, point)

        point = context.find_treasure(SECOND_MAP)
        self.assertEqual(43, point)

        point = context.find_treasure(WRONG_MAP)
        self.assertIsNone(point)

    def test_linked_list_finder(self):
        context = TreasureLinkedList()

        point = context.find_treasure(FIRST_MAP)
        self.assertEqual(52, point)

        context.clear()
        point = context.find_treasure(SECOND_MAP)
        self.assertEqual(43, point)

        context.clear()
        point = context.find_treasure(WRONG_MAP)
        self.assertIsNone(point)

    def test_recursion_finder(self):
        point = recursion_find_treasure(treasure_map=FIRST_MAP)
        self.assertEqual(52, point)

        point = recursion_find_treasure(treasure_map=SECOND_MAP)
        self.assertEqual(43, point)

        point = recursion_find_treasure(treasure_map=WRONG_MAP)
        self.assertIsNone(point)
