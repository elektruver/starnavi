from patterns import Context
from treasure_maps import FIRST_MAP, SECOND_MAP, WRONG_MAP
from algorithms import (
    CellWalkerFinder,
    graph_find_treasure,
    TreasureLinkedList,
    recursion_find_treasure
)


if __name__ == '__main__':
    # print(graph_find_treasure(FIRST_MAP))
    #
    # context = Context(CellWalkerFinder())
    # print(context.find_treasure(SECOND_MAP))
    #
    # context = Context(TreasureLinkedList())
    # print(context.find_treasure(WRONG_MAP))
    print(recursion_find_treasure(FIRST_MAP))
    print(recursion_find_treasure(SECOND_MAP))
    print(recursion_find_treasure(WRONG_MAP))
    print()
